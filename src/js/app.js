const body = document.getElementById('body');
const btnClosedModal2 = document.getElementById('close_icon_2');
const btnsubmit2 = document.getElementById('closedModal2');
const btnOpenModal2 = document.getElementById('modal_open_2');
const modal2 = document.getElementById('modal_2');
const ulClients = document.getElementById('ul_clients');

const open2 = function (){
    modal2.classList.add('modal-active');
    body.classList.add('main_hidden');
    ulClients.style.display = "none";
}
const closed2 = function (event) {
    event.preventDefault();
    event.stopPropagation();
    modal2.classList.remove('modal-active');
    body.classList.remove('main_hidden');
    ulClients.style.display = "";
}
btnOpenModal2.addEventListener('click', open2);
btnClosedModal2.addEventListener('click', closed2);
btnsubmit2.addEventListener('click', closed2);

const header = document.getElementById("header");
const logo = document.getElementById("topLogo");
const topText = document.getElementsByClassName('header__menu__text');
const dropMenu = document.getElementsByClassName('drop-down-menu__li');
const dropMenuText = document.getElementsByClassName('drop-down-menu__li__text');
let heightHeader = header.offsetHeight;


//modal small start
try {
    const btnOpenModal = document.getElementById('openModal');
    const btnClosedModal = document.getElementById('close_icon');
    const btnsubmit = document.getElementById('closedModal');
    const modal = document.getElementById('modal');


    const open = function (){
        modal.classList.add('modal-active');
        body.classList.add('main_hidden');
    }

    const closed = function (event) {
        event.preventDefault();
        event.stopPropagation();
        modal.classList.remove('modal-active');
        body.classList.remove('main_hidden');
    }
    btnOpenModal.addEventListener('click', open);
    btnClosedModal.addEventListener('click', closed);
    btnsubmit.addEventListener('click', closed);
}
catch (e){
    console.log(e)
}

//modal small end

// top nav menu start

window.onscroll = function () {
    // console.log(document.body.scrollTop);
    // console.log(document.documentElement.scrollTop);
    // console.log(window.innerWidth);
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50){
        header.style.background = "#ffffff"
        logo.style.width = "57px";
        logo.style.height = "55px";
        logo.style.transition = "1s";
        logo.style.top = "5px";
        header.style.opacity = "0.9";
        for(let i = 0; i< topText.length; i++){
            topText[i].classList.add('header__menu__text-scroll')
        }
        for(let i = 0; i< dropMenu.length; i++){
            dropMenu[i].classList.add('drop-down-menu__li-scroll')
        }
        for(let i = 0; i< dropMenuText.length; i++){
            dropMenuText[i].classList.add('drop-down-menu__li__text-scroll')
        }
    }
    else {
        header.style.background = ""
        logo.style.width = "114px";
        logo.style.height = "110px";
        logo.style.top = "10px";
        header.style.opacity = "1";
        for(let i = 0; i< topText.length; i++){
            topText[i].classList.remove('header__menu__text-scroll')
        }
        for(let i = 0; i< dropMenu.length; i++){
            dropMenu[i].classList.remove('drop-down-menu__li-scroll')
        }
        for(let i = 0; i< dropMenuText.length; i++){
            dropMenuText[i].classList.remove('drop-down-menu__li__text-scroll')
        }
    }
};
// top nav menu end


// faq block start

try{
    const openBtnSvg = `<svg  class="btn-svg" width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M10 1L5.5 5L1 1" stroke-width="2"/>
</svg>`;
    const closedBtnSvg = `<svg  class="btn-svg" width="11" height="7" viewBox="0 0 11 7" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M10 6L5.5 2L1 6" stroke-width="2"/>
</svg>`;
    const btnPlace = document.getElementsByClassName('btn_faq');
    const svgInsertToDiv = () => {
        for(let i=0; i < btnPlace.length; i++ ){
            btnPlace[i].innerHTML = openBtnSvg}
    };
    svgInsertToDiv();

    const faqBlock = document.getElementsByClassName('faq_block');

    const idToElem = () =>{
        for (let i = 0; i < faqBlock.length; i++) {
            faqBlock[i].id = i;
            faqBlock[i].addEventListener('click', open)
            for (let j = 0; j <faqBlock[i].children.length; j++){
                if(faqBlock[i].children[j].classList.contains('qst_block')){
                    for (let k = 0; k < faqBlock[i].children[j].children.length; k++ ){
                        if(faqBlock[i].children[j].children[k].classList.contains('btn_faq')){
                            faqBlock[i].children[j].children[k].id = `btn${i}`;
                        }
                    }
                }
                if(faqBlock[i].children[j].classList.contains('box')){
                    faqBlock[i].children[j].id = `box${i}`
                }
            }
        }
    };
    function closed(e){
        const close = document.getElementById(`close${this.dataset.id}`);
        if(close)
        {
            e.stopPropagation();
            const elem = document.getElementById(`${this.dataset.id}`);
            const btn = document.getElementById(`close${this.dataset.id}`);
            const box = document.getElementById(`box${this.dataset.id}`);
            box.classList.add('invisible');
            elem.classList.add('hover_style');
            btn.innerHTML = openBtnSvg;
            btn.id = `btn${this.dataset.id}`;
            btn.classList.remove('btn_closed');
        }
    }
    function open() {
        const elem = document.getElementById(`${this.id}`);
        const btn = document.getElementById(`btn${this.id}`);
        const box = document.getElementById(`box${this.id}`);
        box.classList.remove('invisible');
        elem.classList.remove('hover_style');
        btn.innerHTML = closedBtnSvg;
        btn.classList.add('btn_closed');
        btn.id = `close${this.id}`;
        btn.dataset.id = `${this.id}`;
        const close = document.getElementById(`close${this.id}`);
        close.addEventListener('click', closed);
    }


    idToElem();
}
catch (e){
    console.log(e)
}



// faq block end